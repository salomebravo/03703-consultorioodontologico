package com.consultorioonline.consultorioonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioOnlineApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsultorioOnlineApplication.class, args);
    }

}
