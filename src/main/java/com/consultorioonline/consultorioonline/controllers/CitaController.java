package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Cita;
import com.consultorioonline.consultorioonline.modals.Paciente;
import com.consultorioonline.consultorioonline.services.CitaService;
import com.consultorioonline.consultorioonline.services.OdontologoService;
import com.consultorioonline.consultorioonline.services.PacienteService;
import com.consultorioonline.consultorioonline.services.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CitaController {

    @Autowired
    CitaService citaService;

    @Autowired
    PacienteService pacienteService;

    @Autowired
    OdontologoService odontologoService;

    @Autowired
    ServicioService servicioService;

    @GetMapping({"/cita/list"})
    public String Listar(Model model){
        model.addAttribute("cita", citaService.Listar());
        return "/cita/cita_list";
    }

    @GetMapping({"/cita/create"})
    public String Crear(Model model){
        Cita cita = new Cita();
        model.addAttribute("cita", cita);
        model.addAttribute("paciente", pacienteService.Listar());
        model.addAttribute("odontologo", odontologoService.Listar());
        model.addAttribute("servicio", servicioService.Listar());
        return "/cita/cita_create";
    }

    @PostMapping({"/cita/guardar"})
    public String Guardar(@ModelAttribute Cita cita){
        citaService.Guardar(cita);
        return "redirect:/cita/list";
    }

    @GetMapping({"/cita/edit/{cod}"})
    public String Editar(@PathVariable int cod, Model model){
        Cita cita = citaService.BuscarById(cod);
        model.addAttribute("paciente", pacienteService.Listar());
        model.addAttribute("odontologo", odontologoService.Listar());
        model.addAttribute("servicio", servicioService.Listar());
        model.addAttribute("cita", cita);
        return "/cita/cita_create";
    }

    @GetMapping({"/cita/delete/{cod}"})
    public String Eliminar(@PathVariable int cod){
        citaService.Eliminar(cod);
        return "redirect:/cita/list";
    }

}
