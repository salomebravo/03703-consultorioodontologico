package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Historia_clinica;
import com.consultorioonline.consultorioonline.modals.Paciente;
import com.consultorioonline.consultorioonline.services.Historia_clinicaService;
import com.consultorioonline.consultorioonline.services.PacienteService;
import com.consultorioonline.consultorioonline.services.TratamientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class Historia_clinicaController {

    @Autowired
    Historia_clinicaService hsService;

    @Autowired
    PacienteService pacienteService;

    @Autowired
    TratamientoService tratamientoService;

    @GetMapping({"/historia_clinica/list"})
    public String Listar(Model model){
        model.addAttribute("historia", hsService.Listar());
        return "/historia_clinica/historia_list";
    }

    @GetMapping({"/historia_clinica/create"})
    public String Crear(Model model){
        Historia_clinica historia_clinica = new Historia_clinica();
        model.addAttribute("historia", historia_clinica);
        model.addAttribute("pacientes", pacienteService.Listar());
        model.addAttribute("tratamiento", tratamientoService.Listar());
        return "/historia_clinica/historia_create";
    }

    @PostMapping({"/historia_clinica/guardar"})
    public String Guardar(@ModelAttribute Historia_clinica historia){
        hsService.Guardar(historia);
        return "redirect:/historia_clinica/list";
    }

    @GetMapping({"/historia_clinica/edit/{cod}"})
    public String Editar(@PathVariable int cod, Model model){
        Historia_clinica historia = hsService.BuscarById(cod);
        model.addAttribute("historia", historia);
        return "/historia_clinica/historia_create";
    }

    @GetMapping({"/historia_clinica/delete/{cod}"})
    public String Eliminar(@PathVariable int cod){
        hsService.Eliminar(cod);
        return "redirect:/historia_clinica/list";
    }
}
