package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Odontologo;
import com.consultorioonline.consultorioonline.modals.Usuario;
import com.consultorioonline.consultorioonline.services.OdontologoService;
import com.consultorioonline.consultorioonline.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class OdontologoController {

    @Autowired
    OdontologoService odontologoService;

    @Autowired
    UsuarioService usuarioService;

    @GetMapping({"/odontologo/list"})
    public String Listar(Model model){
        model.addAttribute("odontologo", odontologoService.Listar());
        return "/odontologo/odontologo_list";
    }

    @GetMapping({"/odontologo/create"})
    public String Crear(Model model){
        Odontologo odontologo = new Odontologo();
        Usuario usuario = usuarioService.BuscarUltimo();
        model.addAttribute("usuarios", usuario);
        model.addAttribute("odontologo", odontologo);
        return "/odontologo/odontologo_create";
    }

    @PostMapping("/odontologo/guardar")
    public String Guardar(@ModelAttribute Odontologo odontologo){
        odontologoService.Guardar(odontologo);
        return "redirect:/odontologo/list";
    }

    @GetMapping({"/odontologo/edit/{id}"})
    public String Editar(@PathVariable int id, Model model){
        Odontologo odontologo = odontologoService.BuscarById(id);
        model.addAttribute("odontologo", odontologo);
        return "/odontologo/odontologo_edit";
    }

    @GetMapping({"/odontologo/delete/{id}"})
    public String Eliminar(@PathVariable int id){
        odontologoService.Eliminar(id);
        return "redirect:/odontologo/list";
    }
}
