package com.consultorioonline.consultorioonline.controllers;
import com.consultorioonline.consultorioonline.modals.Paciente;
import com.consultorioonline.consultorioonline.modals.Usuario;
import com.consultorioonline.consultorioonline.services.PacienteService;
import com.consultorioonline.consultorioonline.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PacienteController {
    @Autowired
    PacienteService service;

    @Autowired
    UsuarioService usuarioService;

    //Mapear la ruta de la entidad = paciente
    @GetMapping({"paciente/list"})
    public String listar(Model model) {
        model.addAttribute("pacientes", service.Listar());
        return "paciente/paciente_list";
    }


    @GetMapping({"/paciente/create"})
    public String Nuevo(Model model) {
        Paciente paciente = new Paciente();
        Usuario usuario = usuarioService.BuscarUltimo();
        model.addAttribute("paciente", paciente);
        model.addAttribute("usuarios", usuario);
        return "/paciente/create";
    }

    @PostMapping({"/paciente/guardar"})
    public String Guardar(@ModelAttribute("paciente") Paciente paciente){
        service.Guardar(paciente);
        return "redirect:/paciente/list";
    }

    @GetMapping({"/paciente/edit/{id}"})
    public String Editar(@PathVariable int id, Model model){
        Paciente paciente=service.BuscarById((id));
        model.addAttribute("paciente", paciente);
        return "paciente/paciente_edit";
    }

    @GetMapping({"/paciente/eliminar/{id}"})
    public String Eliminar(@PathVariable int id){
        service.Eliminar(id);
        return "redirect:/paciente/list";
    }
}