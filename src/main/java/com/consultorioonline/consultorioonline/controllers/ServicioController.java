package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Servicio;
import com.consultorioonline.consultorioonline.services.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class ServicioController {

    @Autowired
    ServicioService servicioService;

    @GetMapping({"/servicio/list"})
    public String Listar(Model model){
        model.addAttribute("servicio", servicioService.Listar());
        return "/servicio/servicio_list";
    }

    @GetMapping({"/servicio/create"})
    public String Crear(Model model){
        Servicio servicio = new Servicio();
        model.addAttribute("servicio", servicio);
        return "/servicio/servicio_create";
    }

    @PostMapping({"/servicio/guardar"})
    public String Guardar(@ModelAttribute("servicio") Servicio servicio){
        servicioService.Guardar(servicio);
        return "redirect:/servicio/list";
    }

    @GetMapping({"/servicio/edit/{cod}"})
    public String Editar(@PathVariable int cod, Model model){
        Servicio servicio = servicioService.BuscarById(cod);
        model.addAttribute("servicio", servicio);
        return "/servicio/servicio_create";
    }

    @GetMapping({"/servicio/delete/{cod}"})
    public String Eliminar(@PathVariable int cod){
        servicioService.Eliminar(cod);
        return "redirect:/servicio/list";
    }
}
