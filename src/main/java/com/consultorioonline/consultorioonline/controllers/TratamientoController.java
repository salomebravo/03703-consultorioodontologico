package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Tratamiento;
import com.consultorioonline.consultorioonline.services.TratamientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TratamientoController {

    @Autowired
    TratamientoService tratamientoService;

    @GetMapping({"/tratamiento/list"})
    public String Listar(Model model){
        model.addAttribute("tratamiento", tratamientoService.Listar());
        return "/tratamiento/tratamiento_list";
    }

    @GetMapping({"/tratamiento/create"})
    public String Create(Model model){
        Tratamiento tratamiento = new Tratamiento();
        model.addAttribute("tratamiento", tratamiento);
        return "/tratamiento/tratamiento_create";
    }

    @PostMapping({"/tratamiento/guardar"})
    public String Guardar(@ModelAttribute Tratamiento tratamiento){
        tratamientoService.Guardar(tratamiento);
        return "redirect:/tratamiento/list";
    }

    @GetMapping({"/tratamiento/edit/{cod}"})
    public String Editar(@PathVariable int cod, Model model){
        model.addAttribute("tratamiento", tratamientoService.BuscarById(cod));
        return "/tratamiento/tratamiento_create";
    }

    @GetMapping({"/tratamiento/delete/{cod}"})
    public String Eliminar(@PathVariable int cod){
        tratamientoService.Eliminar(cod);
        return "redirect:/tratamiento/list";
    }

}
