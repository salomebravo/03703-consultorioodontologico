package com.consultorioonline.consultorioonline.controllers;

import com.consultorioonline.consultorioonline.modals.Paciente;
import com.consultorioonline.consultorioonline.modals.Usuario;
import com.consultorioonline.consultorioonline.services.PacienteService;
import com.consultorioonline.consultorioonline.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    //Mapear la ruta de la entidad = usuario
    @GetMapping({"/usuario/list"})
    public String listar(Model model) {
        model.addAttribute("usuarios", usuarioService.Listar());
        return "/usuario/usuario_list";
    }

    @GetMapping({"/usuario/create"})
    public String Crear(Model model){
        Usuario usuario = new Usuario();
        model.addAttribute("usuario", usuario);
        return "/usuario/usuario_create";
    }

    @PostMapping({"/usuario/guardar"})
    public String Guardar(@ModelAttribute("usuario") Usuario usuario){
        String pag = "";
        usuarioService.Guardar(usuario);
        if(usuario.getTipo_usu().equals("Paciente")){
            pag = "redirect:/paciente/create";
        } else if(usuario.getTipo_usu().equals("Odontologo")){
            pag = "redirect:/odontologo/create";
        }
        return pag;
    }

    @GetMapping({"/usuario/edit/{cod}"})
    public String Editar(@PathVariable int cod, Model model){
        Usuario usuario = usuarioService.BuscarById(cod);
        model.addAttribute("usuario", usuario);
        return "/usuario/usuario_create";
    }

    @GetMapping({"/usuario/delete/{cod}"})
    public String Eliminar(@PathVariable int cod){
        usuarioService.Eliminar(cod);
        return "redirect:/usuario/list";
    }
}
