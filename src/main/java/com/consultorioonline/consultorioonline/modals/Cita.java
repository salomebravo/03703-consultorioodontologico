package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalTime;

@Setter
@Getter
@Entity
@Table(name = "cita")
public class Cita {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codi_cit;
    private Date fech_cit;
    private LocalTime hora_cit;

    @ManyToOne
    @JoinColumn(name = "iden_odo_cit")
    private Odontologo iden_odo_cit;

    @ManyToOne
    @JoinColumn(name = "iden_pac_cit")
    private Paciente iden_pac_cit;

    @OneToOne
    @JoinColumn(name = "codi_ser_cit")
    private Servicio codi_ser_cit;
}
