package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "historia_clinica")
public class Historia_clinica {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codi_his;
    private String moticons_his;
    private String histmedi_his ;
    private String diag_his;

    @OneToOne
    @JoinColumn(name = "trat_tra_his", foreignKey = @ForeignKey(name = "trat_tra_his", value =ConstraintMode.CONSTRAINT))
    private Tratamiento trat_tra_his;

    @ManyToOne
    @JoinColumn(name = "iden_pac_his", foreignKey = @ForeignKey(name = "iden_pac_his", value =ConstraintMode.CONSTRAINT))
    private Paciente iden_pac_his;
}
