package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "odontologo")
public class Odontologo {
    @Id
    private int iden_odo;
    private String espe_odo;

    @OneToOne
    @JoinColumn(name = "codi_usu_odo")
    private Usuario codi_usu_odo;
}
