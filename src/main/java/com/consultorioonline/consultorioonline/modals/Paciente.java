package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "paciente")
public class Paciente {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int iden_pac;
    private String dire_pac;
    private String tele_pac;

    @OneToOne
    @JoinColumn(name = "codi_usu_pac" ,foreignKey = @ForeignKey(name = "codi_usu_pac", value =ConstraintMode.CONSTRAINT))
    private Usuario codi_usu_pac;
}

