package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "servicio")
public class Servicio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codi_ser;
    private String nomb_ser;
    private float prec_ser;
}
