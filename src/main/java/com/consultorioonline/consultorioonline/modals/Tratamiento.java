package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tratamiento")
public class Tratamiento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codi_tra;
    private String nomb_tra;
    private float prec_tra;
}
