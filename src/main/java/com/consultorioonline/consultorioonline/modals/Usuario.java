package com.consultorioonline.consultorioonline.modals;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codi_usu;
    private String corre_usu;
    private String cont_usu;
    private String nomb_usu;
    private String apel_usu;
    private String edad_usu;
    private String sexo_usu;
    private String tipo_usu;

}
