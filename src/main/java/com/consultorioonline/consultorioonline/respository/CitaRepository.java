package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Cita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitaRepository extends JpaRepository<Cita, Integer> {
}
