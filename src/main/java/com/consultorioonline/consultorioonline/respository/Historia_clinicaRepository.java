package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Historia_clinica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Historia_clinicaRepository extends JpaRepository<Historia_clinica, Integer> {
}
