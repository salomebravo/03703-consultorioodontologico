package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Odontologo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OdontologoRepository extends JpaRepository<Odontologo, Integer> {
}
