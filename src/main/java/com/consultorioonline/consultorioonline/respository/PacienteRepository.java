package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepository extends JpaRepository<Paciente, Integer> {
}

