package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Servicio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicioRepository extends JpaRepository<Servicio, Integer> {
}
