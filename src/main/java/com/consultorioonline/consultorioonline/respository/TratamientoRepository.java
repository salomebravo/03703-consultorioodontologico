package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Tratamiento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TratamientoRepository extends JpaRepository<Tratamiento, Integer> {
}
