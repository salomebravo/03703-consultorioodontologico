package com.consultorioonline.consultorioonline.respository;

import com.consultorioonline.consultorioonline.modals.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(value = "SELECT * FROM usuario WHERE codi_usu = (SELECT MAX(codi_usu) FROM usuario)", nativeQuery = true)
    public Usuario findlatest();
}
