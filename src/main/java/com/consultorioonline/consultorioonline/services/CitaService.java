package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Cita;
import com.consultorioonline.consultorioonline.respository.CitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CitaService {

    @Autowired
    CitaRepository citaRepository;

    public List<Cita> Listar(){
        return citaRepository.findAll();
    }

    public Cita Guardar(Cita cita){
        return citaRepository.save(cita);
    }

    public Cita BuscarById(Integer cod){
        return citaRepository.findById(cod).get();
    }

    public void Eliminar(Integer cod){
        citaRepository.deleteById(cod);
    }
}
