package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Historia_clinica;
import com.consultorioonline.consultorioonline.respository.Historia_clinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Historia_clinicaService {

    @Autowired
    Historia_clinicaRepository hcRepository;

    public List<Historia_clinica> Listar(){
        return hcRepository.findAll();
    }

    public Historia_clinica Guardar(Historia_clinica historia_clinica){
        return hcRepository.save(historia_clinica);
    }

    public Historia_clinica BuscarById(Integer cod){
        return hcRepository.findById(cod).get();
    }

    public void Eliminar(Integer cod){
        hcRepository.deleteById(cod);
    }
}
