package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Odontologo;
import com.consultorioonline.consultorioonline.respository.OdontologoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OdontologoService {

    @Autowired
    OdontologoRepository odontologoRepository;

    public List<Odontologo> Listar(){
        return odontologoRepository.findAll();
    }

    public Odontologo Guardar(Odontologo odontologo){
        return odontologoRepository.save(odontologo);
    }

    public Odontologo BuscarById(Integer cod){
        return odontologoRepository.findById(cod).get();
    }

    public void Eliminar(Integer cod){
        odontologoRepository.deleteById(cod);
    }
}
