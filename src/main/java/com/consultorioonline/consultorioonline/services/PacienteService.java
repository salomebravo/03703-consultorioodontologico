package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Paciente;
import com.consultorioonline.consultorioonline.respository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacienteService {
    @Autowired
    PacienteRepository repository;
        //CRUD
    //METODO LISTAR
    public List<Paciente> Listar(){
        return repository.findAll();
    }
    //METODO GUARDAR Y ACTUALIZAR CREATE AND UPDATE
    public Paciente  Guardar(Paciente paciente){
        return repository.save(paciente);
    }
    //METODO BUSCAR POR ID
    public Paciente BuscarById(Integer id){
        return repository.findById(id).get();
    }
    //METODO ELIMINAR
    public void Eliminar(Integer id){
        repository.deleteById(id);
    }
}
