package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Servicio;
import com.consultorioonline.consultorioonline.respository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioService {

    @Autowired
    ServicioRepository servicioRepository;

    public List<Servicio> Listar(){
        return servicioRepository.findAll();
    }

    public Servicio Guardar(Servicio servicio){
        return servicioRepository.save(servicio);
    }

    public Servicio BuscarById(Integer cod){
        return servicioRepository.findById(cod).get();
    }

    public void Eliminar(Integer cod){
        servicioRepository.deleteById(cod);
    }
}
