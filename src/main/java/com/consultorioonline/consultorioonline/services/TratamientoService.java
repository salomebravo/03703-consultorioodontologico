package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Tratamiento;
import com.consultorioonline.consultorioonline.respository.TratamientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TratamientoService {

    @Autowired
    TratamientoRepository tratamientoRepository;

    public List<Tratamiento> Listar(){
        return tratamientoRepository.findAll();
    }

    public Tratamiento Guardar(Tratamiento tratamiento){
        return tratamientoRepository.save(tratamiento);
    }

    public Tratamiento BuscarById(Integer cod){
        return tratamientoRepository.findById(cod).get();
    }

    public void Eliminar(Integer cod){
        tratamientoRepository.deleteById(cod);
    }

}
