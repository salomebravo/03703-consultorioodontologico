package com.consultorioonline.consultorioonline.services;

import com.consultorioonline.consultorioonline.modals.Usuario;
import com.consultorioonline.consultorioonline.respository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository usuarioRepository;

    public List<Usuario> Listar(){
        return usuarioRepository.findAll();
    }

    public Usuario Guardar(Usuario usuario){ return usuarioRepository.save(usuario); }

    public Usuario BuscarById(Integer codigo){ return usuarioRepository.findById(codigo).get(); }

    public Usuario BuscarUltimo(){
        return usuarioRepository.findlatest();
    }

    public void Eliminar(Integer codigo){ usuarioRepository.deleteById(codigo); }

}
