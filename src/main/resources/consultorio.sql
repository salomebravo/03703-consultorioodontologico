-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-09-2022 a las 04:57:24
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultorio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `codi_cit` int(11) NOT NULL COMMENT 'clave primaria y codigo para cada cita',
  `fech_cit` date NOT NULL COMMENT 'fecha de la cita',
  `hora_cit` time NOT NULL COMMENT 'hora de la cita',
  `iden_odo_cit` int(11) NOT NULL COMMENT 'identificacion del odontologo a cargo de la cita',
  `iden_pac_cit` int(11) NOT NULL COMMENT 'paciente quien requiere la cita',
  `codi_ser_cit` int(11) NOT NULL COMMENT 'codigo del servicio en la cita'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`codi_cit`, `fech_cit`, `hora_cit`, `iden_odo_cit`, `iden_pac_cit`, `codi_ser_cit`) VALUES
(123, '2022-09-15', '08:00:00', 312123, 1470258369, 1425),
(124, '2022-09-15', '08:30:00', 312123, 1110552663, 1426),
(125, '2022-09-15', '13:30:00', 312123, 1110123456, 1427);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia_clinica`
--

CREATE TABLE `historia_clinica` (
  `codi_his` int(11) NOT NULL COMMENT 'codigo para cada historia clinica',
  `motiCons_his` text COLLATE utf8_spanish2_ci NOT NULL COMMENT 'motivo por el cual asiste a la consulta',
  `histMedi_his` text COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'historia medica (a que medicamentos es alegico, entre otras)',
  `diag_his` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'diagnostico recetado por el odontologo',
  `trat_tra_his` int(11) DEFAULT NULL COMMENT 'codigo de tratamiento asignado por el odontologo',
  `iden_pac_his` int(11) NOT NULL COMMENT 'codigo del paciente a quien corresponde la historia'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `odontologo`
--

CREATE TABLE `odontologo` (
  `iden_odo` int(11) NOT NULL COMMENT 'codigo de odontologo',
  `espe_odo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'especializacion del odontologo',
  `codi_usu_odo` int(11) NOT NULL COMMENT 'codigo de usuario correspondiente al odontologo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `odontologo`
--

INSERT INTO `odontologo` (`iden_odo`, `espe_odo`, `codi_usu_odo`) VALUES
(312123, 'Dentista general', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `iden_pac` int(11) NOT NULL COMMENT 'identificacion del paciente',
  `dire_pac` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'direccion de residencia del paciente',
  `tele_pac` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'telefono del paciente',
  `codi_usu_pac` int(11) NOT NULL COMMENT 'codigo de usuario para el paciente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`iden_pac`, `dire_pac`, `tele_pac`, `codi_usu_pac`) VALUES
(1110123456, 'Conjunto La Esmeralda Apto 1002 Torre 4', '3216549873', 14),
(1110552663, 'Calle 61c CRA 8va Jardin', '325698214', 12),
(1470258369, 'Barrio Buena Vista Cll 67 # 23-45', '3012583697', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `codi_ser` int(11) NOT NULL COMMENT 'codigo para cada uno de los servicios',
  `nomb_ser` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre del servicio',
  `prec_ser` float NOT NULL COMMENT 'precio del servicio'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`codi_ser`, `nomb_ser`, `prec_ser`) VALUES
(1425, 'Cita General', 40000),
(1426, 'Cita Especializada', 60000),
(1427, 'Cita de Chequeo', 35000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamiento`
--

CREATE TABLE `tratamiento` (
  `codi_tra` int(11) NOT NULL COMMENT 'codigo del tratamiento',
  `nomb_tra` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre del tratamiento',
  `prec_tra` float NOT NULL COMMENT 'precio que tiene el tratamiento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `codi_usu` int(11) NOT NULL COMMENT 'codigo usuario',
  `corre_usu` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'correo de usuario',
  `cont_usu` varchar(20) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'contraseña de usuario',
  `nomb_usu` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre del usuario',
  `apel_usu` varchar(50) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'apellido del usuario',
  `edad_usu` int(11) NOT NULL COMMENT 'edad del usuario',
  `sexo_usu` varchar(1) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'sexo del usuario, masculino (M), femenino (F)',
  `tipo_usu` varchar(13) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de usuario (odontologo, paciente, Administrador)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`codi_usu`, `corre_usu`, `cont_usu`, `nomb_usu`, `apel_usu`, `edad_usu`, `sexo_usu`, `tipo_usu`) VALUES
(11, 'salome@gmail.com', '123123', 'Salomé', 'Bravo', 27, 'F', 'Administrador'),
(12, 'camilo@gmail.com', '123123', 'Camilo', 'Ganem', 23, 'M', 'Paciente'),
(13, 'luis@gmail.com', '123123', 'Luis', 'Bruges', 20, 'M', 'Odontologo'),
(14, 'Sergio@gmail.com', '654321', 'Sergio', 'Canales', 23, 'M', 'Paciente'),
(15, 'sandra@gmail.com', '581436', 'Sandra', 'Vargaz', 36, 'F', 'Paciente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`codi_cit`),
  ADD KEY `codigo_servicio` (`codi_ser_cit`),
  ADD KEY `codigo del odontologo` (`iden_odo_cit`),
  ADD KEY `codigo del paciente` (`iden_pac_cit`);

--
-- Indices de la tabla `historia_clinica`
--
ALTER TABLE `historia_clinica`
  ADD PRIMARY KEY (`codi_his`),
  ADD KEY `codigo del tratamiento` (`trat_tra_his`),
  ADD KEY `codigo del paciente` (`iden_pac_his`);

--
-- Indices de la tabla `odontologo`
--
ALTER TABLE `odontologo`
  ADD PRIMARY KEY (`iden_odo`),
  ADD KEY `codigo del paciente` (`codi_usu_odo`),
  ADD KEY `codigo de su usuario` (`codi_usu_odo`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`iden_pac`),
  ADD KEY `codigo de su usuario` (`codi_usu_pac`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`codi_ser`);

--
-- Indices de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  ADD PRIMARY KEY (`codi_tra`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`codi_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `codi_cit` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clave primaria y codigo para cada cita', AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT de la tabla `historia_clinica`
--
ALTER TABLE `historia_clinica`
  MODIFY `codi_his` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo para cada historia clinica';

--
-- AUTO_INCREMENT de la tabla `odontologo`
--
ALTER TABLE `odontologo`
  MODIFY `iden_odo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo de odontologo', AUTO_INCREMENT=312124;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `codi_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo para cada uno de los servicios', AUTO_INCREMENT=1428;

--
-- AUTO_INCREMENT de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  MODIFY `codi_tra` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo del tratamiento';

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `codi_usu` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo usuario', AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`iden_odo_cit`) REFERENCES `odontologo` (`iden_odo`),
  ADD CONSTRAINT `cita_ibfk_2` FOREIGN KEY (`iden_pac_cit`) REFERENCES `paciente` (`iden_pac`),
  ADD CONSTRAINT `cita_ibfk_3` FOREIGN KEY (`codi_ser_cit`) REFERENCES `servicio` (`codi_ser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historia_clinica`
--
ALTER TABLE `historia_clinica`
  ADD CONSTRAINT `historia_clinica_ibfk_1` FOREIGN KEY (`trat_tra_his`) REFERENCES `tratamiento` (`codi_tra`),
  ADD CONSTRAINT `historia_clinica_ibfk_2` FOREIGN KEY (`iden_pac_his`) REFERENCES `paciente` (`iden_pac`);

--
-- Filtros para la tabla `odontologo`
--
ALTER TABLE `odontologo`
  ADD CONSTRAINT `odontologo_ibfk_1` FOREIGN KEY (`codi_usu_odo`) REFERENCES `usuario` (`codi_usu`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`codi_usu_pac`) REFERENCES `usuario` (`codi_usu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
